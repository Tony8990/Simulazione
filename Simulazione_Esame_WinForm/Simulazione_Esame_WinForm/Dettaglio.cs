﻿using Simulazione_Esame_WinForm.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulazione_Esame_WinForm
{
    public partial class Dettaglio : Form
    {
        string Connection = "Data Source=TONY-HP;Initial Catalog=Simulazione;Integrated Security=True";
        public int IdPost { get; set; }
        public Dettaglio(int id)
        {
            IdPost = id;
            InitializeComponent();
            DiplayData();
        }

        public void DiplayData()
        {
            using (SqlConnection conn = new SqlConnection(Connection))
            {
                SqlCommand cmd = new SqlCommand("[dbo].[sp_getAllPost_ById]", conn);
                cmd.Parameters.AddWithValue("@IdPost", IdPost);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                conn.Open();
                cmd.ExecuteNonQuery();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    textIdPost.Text = dt.Rows[0]["IdPost"].ToString();
                    textTitolo.Text = dt.Rows[0]["Titolo"].ToString();
                    textDescrizione.Text= dt.Rows[0]["Descrizione"].ToString();
                    textUsername.Text= dt.Rows[0]["Username"].ToString();
                    textData.Text= dt.Rows[0]["Data"].ToString();
                }
                conn.Close();
            }
        }






        private void buttonDelete_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Connection))
            {
                SqlCommand cmd = new SqlCommand("[dbo].[sp_DeletePost]", conn);
                cmd.Parameters.AddWithValue("@IdPost", IdPost);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                cmd.ExecuteNonQuery();
               
                conn.Close();
                MessageBox.Show("Record Deleted Successfully!");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Post p = new Post();
            p.Show();
            this.Hide();
        }
    }
}
