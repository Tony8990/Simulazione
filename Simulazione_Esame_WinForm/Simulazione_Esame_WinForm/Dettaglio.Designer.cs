﻿namespace Simulazione_Esame_WinForm
{
    partial class Dettaglio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Idpost = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.textIdPost = new System.Windows.Forms.TextBox();
            this.textTitolo = new System.Windows.Forms.TextBox();
            this.textDescrizione = new System.Windows.Forms.TextBox();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textData = new System.Windows.Forms.TextBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Titolo";
            // 
            // Idpost
            // 
            this.Idpost.AutoSize = true;
            this.Idpost.Location = new System.Drawing.Point(47, 26);
            this.Idpost.Name = "Idpost";
            this.Idpost.Size = new System.Drawing.Size(18, 13);
            this.Idpost.TabIndex = 1;
            this.Idpost.Text = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Descrizione";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "UserName";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(47, 225);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(30, 13);
            this.label.TabIndex = 4;
            this.label.Text = "Data";
            // 
            // textIdPost
            // 
            this.textIdPost.Location = new System.Drawing.Point(199, 26);
            this.textIdPost.Name = "textIdPost";
            this.textIdPost.ReadOnly = true;
            this.textIdPost.Size = new System.Drawing.Size(171, 20);
            this.textIdPost.TabIndex = 5;
            // 
            // textTitolo
            // 
            this.textTitolo.Location = new System.Drawing.Point(199, 71);
            this.textTitolo.Name = "textTitolo";
            this.textTitolo.ReadOnly = true;
            this.textTitolo.Size = new System.Drawing.Size(171, 20);
            this.textTitolo.TabIndex = 6;
            // 
            // textDescrizione
            // 
            this.textDescrizione.Location = new System.Drawing.Point(199, 122);
            this.textDescrizione.Name = "textDescrizione";
            this.textDescrizione.ReadOnly = true;
            this.textDescrizione.Size = new System.Drawing.Size(171, 20);
            this.textDescrizione.TabIndex = 7;
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(199, 177);
            this.textUsername.Name = "textUsername";
            this.textUsername.ReadOnly = true;
            this.textUsername.Size = new System.Drawing.Size(171, 20);
            this.textUsername.TabIndex = 8;
            // 
            // textData
            // 
            this.textData.HideSelection = false;
            this.textData.Location = new System.Drawing.Point(199, 225);
            this.textData.Name = "textData";
            this.textData.ReadOnly = true;
            this.textData.Size = new System.Drawing.Size(171, 20);
            this.textData.TabIndex = 9;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(250, 290);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(120, 35);
            this.buttonDelete.TabIndex = 10;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 21);
            this.button1.TabIndex = 11;
            this.button1.Text = "<-";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dettaglio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 337);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.textData);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.textDescrizione);
            this.Controls.Add(this.textTitolo);
            this.Controls.Add(this.textIdPost);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Idpost);
            this.Controls.Add(this.label1);
            this.Name = "Dettaglio";
            this.Text = "Dettaglio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Idpost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox textIdPost;
        private System.Windows.Forms.TextBox textTitolo;
        private System.Windows.Forms.TextBox textDescrizione;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textData;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button button1;
    }
}