﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulazione_Esame_WinForm
{
    public partial class Modifica : Form
    {
        string Connection = "Data Source=TONY-HP;Initial Catalog=Simulazione;Integrated Security=True";
        public int IdPost { get; set; }
        public Modifica(int id)
        {
            IdPost = id;
           
            InitializeComponent();
            DiplayData();
        }
        
       
        public void DiplayData()
        {
            using (SqlConnection conn = new SqlConnection(Connection))
            {
                SqlCommand cmd = new SqlCommand("[dbo].[sp_getAllPost_ById]", conn);
                cmd.Parameters.AddWithValue("@IdPost", IdPost);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                conn.Open();
                cmd.ExecuteNonQuery();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    textIdPost.Text = dt.Rows[0]["IdPost"].ToString();
                    textTitolo.Text = dt.Rows[0]["Titolo"].ToString();
                    textDescrizione.Text = dt.Rows[0]["Descrizione"].ToString();
                    textUsername.Text = dt.Rows[0]["Username"].ToString();
                    textData.Text = dt.Rows[0]["Data"].ToString();
                }
                conn.Close();
            }
        }


        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            
                using (SqlConnection conn = new SqlConnection(Connection))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[sp_UpdatePost]", conn);
                    cmd.Parameters.AddWithValue("@IdPost", IdPost);

                    cmd.Parameters.AddWithValue("@Titolo", textTitolo.Text);
                    cmd.Parameters.AddWithValue("@Descrizione", textDescrizione.Text);
                    cmd.Parameters.AddWithValue("@Username", textUsername.Text);
                    cmd.Parameters.AddWithValue("@Data", Convert.ToDateTime(textData.Text));

                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    MessageBox.Show("Record Modificato Successfully!");
                    conn.Close();
                }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Post p = new Post();
            p.Show();
            this.Hide();
        }
    }
}
