﻿namespace Simulazione_Esame_WinForm
{
    partial class Post
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idPostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titoloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descrizioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spgetAllPostBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.simulazioneDataSet = new Simulazione_Esame_WinForm.SimulazioneDataSet();
            this.spgetallpostBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.blogTonyDataSet = new Simulazione_Esame_WinForm.BlogTonyDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.sp_get_all_postTableAdapter = new Simulazione_Esame_WinForm.BlogTonyDataSetTableAdapters.sp_get_all_postTableAdapter();
            this.sp_getAllPostTableAdapter = new Simulazione_Esame_WinForm.SimulazioneDataSetTableAdapters.sp_getAllPostTableAdapter();
            this.buttonAggiungi = new System.Windows.Forms.Button();
            this.buttonModifica = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetAllPostBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simulazioneDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetallpostBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blogTonyDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idPostDataGridViewTextBoxColumn,
            this.titoloDataGridViewTextBoxColumn,
            this.descrizioneDataGridViewTextBoxColumn,
            this.dataDataGridViewTextBoxColumn,
            this.usernameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.spgetAllPostBindingSource1;
            this.dataGridView1.Location = new System.Drawing.Point(38, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(843, 348);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
            // 
            // idPostDataGridViewTextBoxColumn
            // 
            this.idPostDataGridViewTextBoxColumn.DataPropertyName = "IdPost";
            this.idPostDataGridViewTextBoxColumn.HeaderText = "IdPost";
            this.idPostDataGridViewTextBoxColumn.Name = "idPostDataGridViewTextBoxColumn";
            this.idPostDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // titoloDataGridViewTextBoxColumn
            // 
            this.titoloDataGridViewTextBoxColumn.DataPropertyName = "Titolo";
            this.titoloDataGridViewTextBoxColumn.HeaderText = "Titolo";
            this.titoloDataGridViewTextBoxColumn.Name = "titoloDataGridViewTextBoxColumn";
            // 
            // descrizioneDataGridViewTextBoxColumn
            // 
            this.descrizioneDataGridViewTextBoxColumn.DataPropertyName = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn.HeaderText = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn.Name = "descrizioneDataGridViewTextBoxColumn";
            // 
            // dataDataGridViewTextBoxColumn
            // 
            this.dataDataGridViewTextBoxColumn.DataPropertyName = "Data";
            this.dataDataGridViewTextBoxColumn.HeaderText = "Data";
            this.dataDataGridViewTextBoxColumn.Name = "dataDataGridViewTextBoxColumn";
            // 
            // usernameDataGridViewTextBoxColumn
            // 
            this.usernameDataGridViewTextBoxColumn.DataPropertyName = "Username";
            this.usernameDataGridViewTextBoxColumn.HeaderText = "Username";
            this.usernameDataGridViewTextBoxColumn.Name = "usernameDataGridViewTextBoxColumn";
            // 
            // spgetAllPostBindingSource1
            // 
            this.spgetAllPostBindingSource1.DataMember = "sp_getAllPost";
            this.spgetAllPostBindingSource1.DataSource = this.simulazioneDataSet;
            // 
            // simulazioneDataSet
            // 
            this.simulazioneDataSet.DataSetName = "SimulazioneDataSet";
            this.simulazioneDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spgetallpostBindingSource
            // 
            this.spgetallpostBindingSource.DataMember = "sp_get_all_post";
            this.spgetallpostBindingSource.DataSource = this.blogTonyDataSet;
            // 
            // blogTonyDataSet
            // 
            this.blogTonyDataSet.DataSetName = "BlogTonyDataSet";
            this.blogTonyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(626, 394);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 37);
            this.button1.TabIndex = 1;
            this.button1.Text = "Dettaglio";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sp_get_all_postTableAdapter
            // 
            this.sp_get_all_postTableAdapter.ClearBeforeFill = true;
            // 
            // sp_getAllPostTableAdapter
            // 
            this.sp_getAllPostTableAdapter.ClearBeforeFill = true;
            // 
            // buttonAggiungi
            // 
            this.buttonAggiungi.Location = new System.Drawing.Point(413, 394);
            this.buttonAggiungi.Name = "buttonAggiungi";
            this.buttonAggiungi.Size = new System.Drawing.Size(117, 37);
            this.buttonAggiungi.TabIndex = 2;
            this.buttonAggiungi.Text = "Aggiungi";
            this.buttonAggiungi.UseVisualStyleBackColor = true;
            this.buttonAggiungi.Click += new System.EventHandler(this.buttonAggiungi_Click);
            // 
            // buttonModifica
            // 
            this.buttonModifica.Location = new System.Drawing.Point(123, 394);
            this.buttonModifica.Name = "buttonModifica";
            this.buttonModifica.Size = new System.Drawing.Size(117, 37);
            this.buttonModifica.TabIndex = 3;
            this.buttonModifica.Text = "Modifica";
            this.buttonModifica.UseVisualStyleBackColor = true;
            this.buttonModifica.Click += new System.EventHandler(this.buttonModifica_Click);
            // 
            // Post
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 452);
            this.Controls.Add(this.buttonModifica);
            this.Controls.Add(this.buttonAggiungi);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Post";
            this.Text = "Post";
            this.Load += new System.EventHandler(this.Post_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetAllPostBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simulazioneDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetallpostBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blogTonyDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private BlogTonyDataSet blogTonyDataSet;
        private System.Windows.Forms.BindingSource spgetallpostBindingSource;
        private BlogTonyDataSetTableAdapters.sp_get_all_postTableAdapter sp_get_all_postTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titoloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descrizioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameDataGridViewTextBoxColumn;
        private SimulazioneDataSet simulazioneDataSet;
        private System.Windows.Forms.BindingSource spgetAllPostBindingSource1;
        private SimulazioneDataSetTableAdapters.sp_getAllPostTableAdapter sp_getAllPostTableAdapter;
        private System.Windows.Forms.Button buttonAggiungi;
        private System.Windows.Forms.Button buttonModifica;
    }
}