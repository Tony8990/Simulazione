﻿namespace Simulazione_Esame_WinForm
{
    partial class Modifica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textData = new System.Windows.Forms.TextBox();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textDescrizione = new System.Windows.Forms.TextBox();
            this.textTitolo = new System.Windows.Forms.TextBox();
            this.textIdPost = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Idpost = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 317);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 21);
            this.button1.TabIndex = 23;
            this.button1.Text = "<-";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(258, 303);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(120, 35);
            this.buttonUpdate.TabIndex = 22;
            this.buttonUpdate.Text = "Modifica";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textData
            // 
            this.textData.HideSelection = false;
            this.textData.Location = new System.Drawing.Point(207, 238);
            this.textData.Name = "textData";
            this.textData.Size = new System.Drawing.Size(171, 20);
            this.textData.TabIndex = 21;
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(207, 190);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(171, 20);
            this.textUsername.TabIndex = 20;
            // 
            // textDescrizione
            // 
            this.textDescrizione.Location = new System.Drawing.Point(207, 135);
            this.textDescrizione.Name = "textDescrizione";
            this.textDescrizione.Size = new System.Drawing.Size(171, 20);
            this.textDescrizione.TabIndex = 19;
            // 
            // textTitolo
            // 
            this.textTitolo.Location = new System.Drawing.Point(207, 84);
            this.textTitolo.Name = "textTitolo";
            this.textTitolo.Size = new System.Drawing.Size(171, 20);
            this.textTitolo.TabIndex = 18;
            // 
            // textIdPost
            // 
            this.textIdPost.Location = new System.Drawing.Point(207, 39);
            this.textIdPost.Name = "textIdPost";
            this.textIdPost.ReadOnly = true;
            this.textIdPost.Size = new System.Drawing.Size(171, 20);
            this.textIdPost.TabIndex = 17;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(55, 238);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(30, 13);
            this.label.TabIndex = 16;
            this.label.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "UserName";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Descrizione";
            // 
            // Idpost
            // 
            this.Idpost.AutoSize = true;
            this.Idpost.Location = new System.Drawing.Point(55, 39);
            this.Idpost.Name = "Idpost";
            this.Idpost.Size = new System.Drawing.Size(18, 13);
            this.Idpost.TabIndex = 13;
            this.Idpost.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Titolo";
            // 
            // Modifica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 376);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.textData);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.textDescrizione);
            this.Controls.Add(this.textTitolo);
            this.Controls.Add(this.textIdPost);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Idpost);
            this.Controls.Add(this.label1);
            this.Name = "Modifica";
            this.Text = "Modifica";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.TextBox textData;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textDescrizione;
        private System.Windows.Forms.TextBox textTitolo;
        private System.Windows.Forms.TextBox textIdPost;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Idpost;
        private System.Windows.Forms.Label label1;
    }
}