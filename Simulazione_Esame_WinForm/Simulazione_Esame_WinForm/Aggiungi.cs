﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulazione_Esame_WinForm
{
    public partial class Aggiungi : Form
    {
        string Connection = "Data Source=TONY-HP;Initial Catalog=Simulazione;Integrated Security=True";
        public Aggiungi()
        {
            InitializeComponent();
        }

        private void buttonAggiungi_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Connection))
            {
                SqlCommand cmd = new SqlCommand("[dbo].[sp_InsertPost]", conn);
                cmd.Parameters.AddWithValue("@Titolo", textTitolo.Text);
                cmd.Parameters.AddWithValue("@Descrizione", textDescrizione.Text);
                cmd.Parameters.AddWithValue("@Username", textUsername.Text);
                cmd.Parameters.AddWithValue("@Data", Convert.ToDateTime(textData.Text));

                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                cmd.ExecuteNonQuery();

                conn.Close();
                MessageBox.Show("Record Aggiunto Successfully!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
               Post p = new Post();
               p.Show();
               this.Hide();
            
        }

        


    }
}
