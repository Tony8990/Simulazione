﻿namespace Simulazione_Esame_WinForm
{
    partial class Aggiungi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.buttonAggiungi = new System.Windows.Forms.Button();
            this.textData = new System.Windows.Forms.TextBox();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textDescrizione = new System.Windows.Forms.TextBox();
            this.textTitolo = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(37, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 21);
            this.button1.TabIndex = 23;
            this.button1.Text = "<-";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonAggiungi
            // 
            this.buttonAggiungi.Location = new System.Drawing.Point(262, 299);
            this.buttonAggiungi.Name = "buttonAggiungi";
            this.buttonAggiungi.Size = new System.Drawing.Size(120, 35);
            this.buttonAggiungi.TabIndex = 22;
            this.buttonAggiungi.Text = "Aggiungi";
            this.buttonAggiungi.UseVisualStyleBackColor = true;
            this.buttonAggiungi.Click += new System.EventHandler(this.buttonAggiungi_Click);
            // 
            // textData
            // 
            this.textData.HideSelection = false;
            this.textData.Location = new System.Drawing.Point(211, 209);
            this.textData.Name = "textData";
            this.textData.Size = new System.Drawing.Size(171, 20);
            this.textData.TabIndex = 21;
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(211, 161);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(171, 20);
            this.textUsername.TabIndex = 20;
            // 
            // textDescrizione
            // 
            this.textDescrizione.Location = new System.Drawing.Point(211, 106);
            this.textDescrizione.Name = "textDescrizione";
            this.textDescrizione.Size = new System.Drawing.Size(171, 20);
            this.textDescrizione.TabIndex = 19;
            // 
            // textTitolo
            // 
            this.textTitolo.Location = new System.Drawing.Point(211, 55);
            this.textTitolo.Name = "textTitolo";
            this.textTitolo.Size = new System.Drawing.Size(171, 20);
            this.textTitolo.TabIndex = 18;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(59, 209);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(30, 13);
            this.label.TabIndex = 16;
            this.label.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "UserName";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Descrizione";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Titolo";
            // 
            // Aggiungi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 369);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonAggiungi);
            this.Controls.Add(this.textData);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.textDescrizione);
            this.Controls.Add(this.textTitolo);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "Aggiungi";
            this.Text = "Aggiungi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonAggiungi;
        private System.Windows.Forms.TextBox textData;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textDescrizione;
        private System.Windows.Forms.TextBox textTitolo;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}