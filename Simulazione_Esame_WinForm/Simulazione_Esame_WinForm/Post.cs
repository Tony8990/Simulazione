﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulazione_Esame_WinForm
{
    public partial class Post : Form
    {
        public Post()
        {
            InitializeComponent();
        }
        int ID = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if ( ID!= 0)
            {
                Dettaglio Det = new Dettaglio(ID);
                Det.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Select a Id from table");
            }
        }

        private void Post_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'simulazioneDataSet.sp_getAllPost'. È possibile spostarla o rimuoverla se necessario.
            this.sp_getAllPostTableAdapter.Fill(this.simulazioneDataSet.sp_getAllPost);
            // TODO: questa riga di codice carica i dati nella tabella 'blogTonyDataSet.sp_get_all_post'. È possibile spostarla o rimuoverla se necessario.
            this.sp_get_all_postTableAdapter.Fill(this.blogTonyDataSet.sp_get_all_post);

        }
        
        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void buttonAggiungi_Click(object sender, EventArgs e)
        {
            Aggiungi Agg = new Aggiungi();
            Agg.Show();
            this.Hide();
        }

        private void buttonModifica_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                Modifica Mod = new Modifica(ID);
                Mod.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Please Select a post");  
            }
            
        }
    }
}
